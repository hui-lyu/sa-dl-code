close all;
clear;
clc;

rng('default');

kL2_origin=2000/10;
k01_origin=20000;
k12_origin=10000;
k23_origin=3330;
k30_origin=1000;
kAB1_origin=3500;
kAB2_origin=1750;
kfB_origin=250;
kfLF_origin=100;
kLHC_origin=2300;
kfHC_origin=100;
kfF_origin=100;
kfct_origin=100;
kfNDH_origin=1;
kL1_origin=2000/10;
kfR_origin=200;
kfX_origin=200;
kfFd_origin=5;

percent=0.2;

kL2_range=[kL2_origin*(1-percent) kL2_origin*(1+percent)];
k01_range=[k01_origin*(1-percent) k01_origin*(1+percent)];
k12_range=[k12_origin*(1-percent) k12_origin*(1+percent)];
k23_range=[k23_origin*(1-percent) k23_origin*(1+percent)];
k30_range=[k30_origin*(1-percent) k30_origin*(1+percent)];
kAB1_range=[kAB1_origin*(1-percent) kAB1_origin*(1+percent)];
kAB2_range=[kAB2_origin*(1-percent) kAB2_origin*(1+percent)];
kfB_range=[kfB_origin*(1-percent) kfB_origin*(1+percent)];
kfLF_range=[kfLF_origin*(1-percent) kfLF_origin*(1+percent)];
kLHC_range=[kLHC_origin*(1-percent) kLHC_origin*(1+percent)];
kfHC_range=[kfHC_origin*(1-percent) kfHC_origin*(1+percent)];
kfF_range=[kfF_origin*(1-percent) kfF_origin*(1+percent)];
kfct_range=[kfct_origin*(1-percent) kfct_origin*(1+percent)];
kfNDH_range=[kfNDH_origin*(1-percent) kfNDH_origin*(1+percent)];
kL1_range=[kL1_origin*(1-percent) kL1_origin*(1+percent)];
kfR_range=[kfR_origin*(1-percent) kfR_origin*(1+percent)];
kfX_range=[kfX_origin*(1-percent) kfX_origin*(1+percent)];
kfFd_range=[kfFd_origin*(1-percent) kfFd_origin*(1+percent)];

PAB=1;
PAmB=0;
PABm=0;
PAmBm=0;
PABmm=0;
PAmBmm=0;
PpAB=0;
PpAmB=0;
PpABm=0;
PpAmBm=0;
PpABmm=0;
PpAmBmm=0;
S0=0.25;
S1=0.75;
S2=0;
S3=0;
PQ=2.5;
PQH=2.5;
LHF=1;
LmHF=0;
LHmF=0;
LHFm=0;
LmHmF=0;
LmHFm=0;
LHmFm=0;
LHmmF=0;
LmHmFm=0;
LmHmmF=0;
LHmmFm=0;
LmHmmFm=0;
PC=3;
PCp=0;
Fd=3;
Fdm=0;
RX=0.63;
RXm=0;
RpX=0;
RpXm=0;
FNRi=3;
FNRa=0;
FNRam=0;
FNRamm=0;
PQm=0;

x0=[PAB;PAmB;PABm;PAmBm;PABmm;PAmBmm;...
    PpAB;PpAmB;PpABm;PpAmBm;PpABmm;PpAmBmm;...
    S0;S1;S2;S3;...
    PQ;PQH;...
    LHF;LmHF;LHmF;LHFm;LmHmF;LmHFm;LHmFm;LHmmF;LmHmFm;LmHmmF;LHmmFm;LmHmmFm;...
    PC;PCp;...
    Fd;Fdm;...
    RX;RXm;RpX;RpXm;...
    FNRi;FNRa;FNRam;FNRamm;...
    PQm];

%% Perform Morris Sensitivity Analysis

num_levels = 4;
delta = num_levels / (2*(num_levels -1));

kL2_samples = linspace(0, 1, num_levels)';
k01_samples = linspace(0, 1, num_levels)';
k12_samples = linspace(0, 1, num_levels)';
k23_samples = linspace(0, 1, num_levels)';
k30_samples = linspace(0, 1, num_levels)';
kAB1_samples = linspace(0, 1, num_levels)';
kAB2_samples = linspace(0, 1, num_levels)';
kfB_samples = linspace(0, 1, num_levels)';
kfLF_samples = linspace(0, 1, num_levels)';
kLHC_samples = linspace(0, 1, num_levels)';
kfHC_samples = linspace(0, 1, num_levels)';
kfF_samples = linspace(0, 1, num_levels)';
kfct_samples = linspace(0, 1, num_levels)';
kfNDH_samples = linspace(0, 1, num_levels)';
kL1_samples = linspace(0, 1, num_levels)';
kfR_samples = linspace(0, 1, num_levels)';
kfX_samples = linspace(0, 1, num_levels)';
kfFd_samples = linspace(0, 1, num_levels)';

num_parameters = 18; 

parameter_samples_matrix = [kL2_samples, k01_samples, k12_samples, k23_samples, k30_samples, kAB1_samples, kAB2_samples, kfB_samples, kfLF_samples, kLHC_samples, kfHC_samples, kfF_samples, kfct_samples, kfNDH_samples, kL1_samples, kfR_samples, kfX_samples, kfFd_samples];

x_star_matrix = zeros(num_levels, num_parameters);

for i = 1:num_parameters
    x_star_matrix(:, i) = datasample(parameter_samples_matrix(:, i), num_levels, 'Replace', false);
end

B_matrix = tril(ones(19, 18), -1);

J_matrix_19times18 = ones(19, 18);
J_matrix_19times1 = ones(19, 1);

D_star_matrix = diag(2 * randi([0, 1], 18, 1) - 1);
% sequence = ones(1, 18);
% sequence(2:2:end) = -1;
% D_star_matrix = diag(sequence);

P_star_matrix = eye(18); 
% P_star_matrix = P_star(:, randperm(18));

B_star_matrix = (J_matrix_19times1*x_star_matrix(4,:)+delta/2*((2*B_matrix-J_matrix_19times18)*D_star_matrix+J_matrix_19times18))*P_star_matrix;

num_Bstar_parameters = size(B_star_matrix, 2);
num_Bstar_samples = size(B_star_matrix, 1);

mapped_values_matrix = zeros(num_Bstar_samples, num_Bstar_parameters);

param_ranges = {kL2_range, k01_range, k12_range, k23_range, k30_range, kAB1_range, kAB2_range, kfB_range, kfLF_range, kLHC_range, kfHC_range, kfF_range, kfct_range, kfNDH_range, kL1_range, kfR_range, kfX_range, kfFd_range};

for i = 1:num_Bstar_parameters
    mapped_values_matrix(:, i) = param_ranges{i}(1) + B_star_matrix(:, i) * (param_ranges{i}(2) - param_ranges{i}(1));
end

t=0:1e-4:1;

qyFtotal_values = zeros(num_Bstar_samples, length(t));

for i = 1:num_Bstar_samples
    
        [t, x] = ode15s(@(t, x) ODE_MORRIS_GLOBALf_CFR_DL(t, x, mapped_values_matrix(i,:)), t, x0);
        
        PSIIm = x(:,2) + x(:,4) + x(:,6) + x(:,8) + x(:,10) + x(:,12);
        PSIIo = x(:,1) + x(:,3) + x(:,5) + x(:,7) + x(:,9) + x(:,11);
        PSIm = x(:,36) + x(:,38);
        qyF_PSIIopen = 0.02;
        qyF_PSIIclosed = 0.08;
        qyFPSII = qyF_PSIIopen * PSIIo + qyF_PSIIclosed * PSIIm;
        qyF_PSIclosed = 0.15 * qyF_PSIIclosed;
        qyFPSI = qyF_PSIclosed * PSIm;
        qyFtotal = qyFPSII + qyFPSI;
        
        qyFtotal_values(i, :) = qyFtotal;
end

qyFtotal_values_transposed = qyFtotal_values';

% 创建一个新的矩阵，大小为[10001, 20]
new_matrix = zeros(10001, 20);

% 将时间序列t插入到新矩阵的第一列
new_matrix(:, 1) = t';

% 将原始数据从第一列复制到新矩阵的第二列到第二十列
for i = 1:19
    new_matrix(:, i+1) = qyFtotal_values_transposed(:, i);
end

% 更新qyFtotal_values_transposed
qyFtotal_values_transposed = new_matrix;

xlswrite('qyFtotal_values.xlsx', qyFtotal_values_transposed, 'Sheet1');

percentiles = prctile(qyFtotal_values, [5, 50, 95]);

%% Calculate Morris Indices

EE_kL2_matrix = zeros(length(t), 1);
EE_k01_matrix = zeros(length(t), 1);
EE_k12_matrix = zeros(length(t), 1);
EE_k23_matrix = zeros(length(t), 1);
EE_k30_matrix = zeros(length(t), 1);
EE_kAB1_matrix = zeros(length(t), 1);
EE_kAB2_matrix = zeros(length(t), 1);
EE_kfB_matrix = zeros(length(t), 1);
EE_kfLF_matrix = zeros(length(t), 1);
EE_kLHC_matrix = zeros(length(t), 1);
EE_kfHC_matrix = zeros(length(t), 1);
EE_kfF_matrix = zeros(length(t), 1);
EE_kfct_matrix = zeros(length(t), 1);
EE_kfNDH_matrix = zeros(length(t), 1);
EE_kL1_matrix = zeros(length(t), 1);
EE_kfR_matrix = zeros(length(t), 1);
EE_kfX_matrix = zeros(length(t), 1);
EE_kfFd_matrix = zeros(length(t), 1);

if B_star_matrix(2, 1) > B_star_matrix(1, 1)
    EE_kL2_matrix(:, 1) = (qyFtotal_values(2, :) - qyFtotal_values(1, :)) / delta;
else
    EE_kL2_matrix(:, 1) = (qyFtotal_values(1, :) - qyFtotal_values(2, :)) / delta;
end

if B_star_matrix(3, 2) > B_star_matrix(2, 2)
    EE_k01_matrix(:, 1) = (qyFtotal_values(3, :) - qyFtotal_values(2, :)) / delta;
else
    EE_k01_matrix(:, 1) = (qyFtotal_values(2, :) - qyFtotal_values(3, :)) / delta;
end

if B_star_matrix(4, 3) > B_star_matrix(3, 3)
    EE_k12_matrix(:, 1) = (qyFtotal_values(4, :) - qyFtotal_values(3, :)) / delta;
else
    EE_k12_matrix(:, 1) = (qyFtotal_values(3, :) - qyFtotal_values(4, :)) / delta;
end

if B_star_matrix(5, 4) > B_star_matrix(4, 4)
    EE_k23_matrix(:, 1) = (qyFtotal_values(5, :) - qyFtotal_values(4, :)) / delta;
else
    EE_k23_matrix(:, 1) = (qyFtotal_values(4, :) - qyFtotal_values(5, :)) / delta;
end

if B_star_matrix(6, 5) > B_star_matrix(5, 5)
    EE_k30_matrix(:, 1) = (qyFtotal_values(6, :) - qyFtotal_values(5, :)) / delta;
else
    EE_k30_matrix(:, 1) = (qyFtotal_values(5, :) - qyFtotal_values(6, :)) / delta;
end

if B_star_matrix(7, 6) > B_star_matrix(6, 6)
    EE_kAB1_matrix(:, 1) = (qyFtotal_values(7, :) - qyFtotal_values(6, :)) / delta;
else
    EE_kAB1_matrix(:, 1) = (qyFtotal_values(6, :) - qyFtotal_values(7, :)) / delta;
end

if B_star_matrix(8, 7) > B_star_matrix(7, 7)
    EE_kAB2_matrix(:, 1) = (qyFtotal_values(8, :) - qyFtotal_values(7, :)) / delta;
else
    EE_kAB2_matrix(:, 1) = (qyFtotal_values(7, :) - qyFtotal_values(8, :)) / delta;
end

if B_star_matrix(9, 8) > B_star_matrix(8, 8)
    EE_kfB_matrix(:, 1) = (qyFtotal_values(9, :) - qyFtotal_values(8, :)) / delta;
else
    EE_kfB_matrix(:, 1) = (qyFtotal_values(8, :) - qyFtotal_values(9, :)) / delta;
end

if B_star_matrix(10, 9) > B_star_matrix(9, 9)
    EE_kfLF_matrix(:, 1) = (qyFtotal_values(10, :) - qyFtotal_values(9, :)) / delta;
else
    EE_kfLF_matrix(:, 1) = (qyFtotal_values(9, :) - qyFtotal_values(10, :)) / delta;
end

if B_star_matrix(11, 10) > B_star_matrix(10, 10)
    EE_kLHC_matrix(:, 1) = (qyFtotal_values(11, :) - qyFtotal_values(10, :)) / delta;
else
    EE_kLHC_matrix(:, 1) = (qyFtotal_values(10, :) - qyFtotal_values(11, :)) / delta;
end

if B_star_matrix(12, 11) > B_star_matrix(11, 11)
    EE_kfHC_matrix(:, 1) = (qyFtotal_values(12, :) - qyFtotal_values(11, :)) / delta;
else
    EE_kfHC_matrix(:, 1) = (qyFtotal_values(11, :) - qyFtotal_values(12, :)) / delta;
end

if B_star_matrix(13, 12) > B_star_matrix(12, 12)
    EE_kfF_matrix(:, 1) = (qyFtotal_values(13, :) - qyFtotal_values(12, :)) / delta;
else
    EE_kfF_matrix(:, 1) = (qyFtotal_values(12, :) - qyFtotal_values(13, :)) / delta;
end

if B_star_matrix(14, 13) > B_star_matrix(13, 13)
    EE_kfct_matrix(:, 1) = (qyFtotal_values(14, :) - qyFtotal_values(13, :)) / delta;
else
    EE_kfct_matrix(:, 1) = (qyFtotal_values(13, :) - qyFtotal_values(14, :)) / delta;
end

if B_star_matrix(15, 14) > B_star_matrix(14, 14)
    EE_kfNDH_matrix(:, 1) = (qyFtotal_values(15, :) - qyFtotal_values(14, :)) / delta;
else
    EE_kfNDH_matrix(:, 1) = (qyFtotal_values(14, :) - qyFtotal_values(15, :)) / delta;
end

if B_star_matrix(16, 15) > B_star_matrix(15, 15)
    EE_kL1_matrix(:, 1) = (qyFtotal_values(16, :) - qyFtotal_values(15, :)) / delta;
else
    EE_kL1_matrix(:, 1) = (qyFtotal_values(15, :) - qyFtotal_values(16, :)) / delta;
end

if B_star_matrix(17, 16) > B_star_matrix(16, 16)
    EE_kfR_matrix(:, 1) = (qyFtotal_values(17, :) - qyFtotal_values(16, :)) / delta;
else
    EE_kfR_matrix(:, 1) = (qyFtotal_values(16, :) - qyFtotal_values(17, :)) / delta;
end

if B_star_matrix(18, 17) > B_star_matrix(17, 17)
    EE_kfX_matrix(:, 1) = (qyFtotal_values(18, :) - qyFtotal_values(17, :)) / delta;
else
    EE_kfX_matrix(:, 1) = (qyFtotal_values(17, :) - qyFtotal_values(18, :)) / delta;
end

if B_star_matrix(19, 18) > B_star_matrix(18, 18)
    EE_kfFd_matrix(:, 1) = (qyFtotal_values(19, :) - qyFtotal_values(18, :)) / delta;
else
    EE_kfFd_matrix(:, 1) = (qyFtotal_values(18, :) - qyFtotal_values(19, :)) / delta;
end

%% Plot the Results

figure(1);
hold on;

for i = 1:num_Bstar_samples
    plot(t, qyFtotal_values(i, :), 'Color', [0.7, 0.7, 0.7]);
end

median_line = plot(t, percentiles(2, :), 'b', 'LineWidth', 1.5);

quartile_lines_90 = plot(t, [percentiles(1, :); percentiles(3, :)], 'r', 'LineWidth', 1.5);

% percentile_5_interp = interp1(t, percentiles(1, :), t);
% percentile_95_interp = interp1(t, percentiles(3, :), t);

% fill([t, flip(t)], [percentile_5_interp, flip(percentile_95_interp)], [0.2 0.8 0.2], 'FaceAlpha', 0.3);

set(gca, 'XScale', 'log');
xlabel('Time (s)');
ylabel('Simulated Fluorescence Emission (a.u.)');
hold off;

legend([median_line, quartile_lines_90(1)], {'Median (50%)', '5%-95% Range'}, 'Location', 'NorthWest');

legend('boxoff');
hLegend = findobj(gcf, 'Type', 'Legend');
set(hLegend, 'FontSize', 10);

% resolution = 600; 
% print('output_image', '-dpng', ['-r', num2str(resolution)]);
